
function createCard(name, description, pictureUrl,startingDate, endingDate, eventLocation ) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">

        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-heading">${eventLocation}</h6>
          <p class="card-text">${description}</p>
        </div>
        
        <div class="card-footer">
            <p class="card-text> "dance"</p>
            <small class="text-muted">${startingDate} - ${endingDate}</small>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded',async() => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        // console.log(response)
        
        if (!response.ok) {
            console.log(`There as been an error. It's status code is as followed ${response.status}`)
            // throw new Error(`Error retrieving conferences: ${response.status} (${response.statusText})`); // This is how it did it in the branched repository. 
        } else {
            const data = await response.json();  // The json() methods converts it into a JavaScript Object. The await keyword pauses the execution of your code until the response.json() method finishes. 
            // console.log(data);
            
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                // console.log(detailResponse);

                if (detailResponse.ok) {
                  const details = await detailResponse.json();

                  console.log(details)
                   


                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const startDate = new Date(details.conference.starts);
                  const endDate = new Date(details.conference.ends);
                  const formattedStart = startDate.toLocaleDateString();
                  const formattedEnd = endDate.toLocaleDateString();
                  const conferenceLocation = details.conference.location.name;
                  const html = createCard(title, description, pictureUrl,formattedStart, formattedEnd, conferenceLocation);
                  console.log(html);

                  const column = document.querySelector('.col');
                  column.innerHTML += html;
                  
                //   const conferenceDescription = document.querySelector('.card-text')
                //   conferenceDescription.innerHTML = `The conference Details --- ${details.conference.description}}` 
                  
                //   const nameTag = document.querySelector('.card-title')
                //   nameTag.innerHTML = `${details.conference.name}}`;
                }
            }
            
            
        }
    } catch (e) {
        throw new Error(`There was an error receiving the conference details. The status code is ${conference.status}`)
    }
});


// window.addEventListener('DOMContentLoaded',async() => {
//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//         const response = await fetch(url);
//         // console.log(response)
        
//         if (!response.ok) {
//             console.log(`There as been an error. It's status code is as followed ${response.status}`)
//             // throw new Error(`Error retrieving conferences: ${response.status} (${response.statusText})`); // This is how it did it in the branched repository. 
//         } else {
//             const data = await response.json();  // The json() methods converts it into a JavaScript Object. The await keyword pauses the execution of your code until the response.json() method finishes. 
//             // console.log(data);
            
//             const conference = data.conferences[0];
//             // console.log(conference);
//             const nameTag = document.querySelector('.card-title')
//             nameTag.innerHTML = conference.name;


//             const detailsUrl = `http://localhost:8000${conference.href}`;
            
//             const detailResponse = await fetch(detailsUrl)
//             console.log(detailResponse)
            
//             if (detailResponse.ok) {
//                 const detailData = await detailResponse.json()
//                 console.log(detailData)
                
//                 const conferenceDescription = document.querySelector('.card-text')
//                 conferenceDescription.innerHTML = `The conference Details --- ${detailData.conference.description}}` 

//                 const ImageF= document.querySelector(".card-img-top")
//                 ImageF.src = detailData.conference.location.picture_url
//                 // const ImagePath.src = detailData.conference.location.picture_url;
                
//                 // ImageF.innerHTML = ImageF
//                 // console.log(ImageF.src)

//             }
//         }
//     } catch (e) {
//         throw new Error(`There was an error receiving the conference details. The status code is ${conference.status}`)
//     }
// });


